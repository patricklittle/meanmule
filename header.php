<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css?family=Droid+Serif|Montserrat" rel="stylesheet">

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url') ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url') ?>/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url') ?>/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo get_bloginfo('template_url') ?>/manifest.json">
<link rel="mask-icon" href="<?php echo get_bloginfo('template_url') ?>/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#000000">
<meta name="google-site-verification" content="6u6l8uETM7o-97NsyazvnxvZW0eluI-TkfKO6pfkKk0" />

<meta property="og:site_name" content="<?php wp_title( '-', true, 'right' ); ?>" />
</head>
<body <?php body_class(); ?>>

    <?php if(is_front_page()) : ?>

    <div id="page" class="hfeed site homepage">
        <header class="header-video">
            <div class="site-header">

    <?php else: ?>

    <div id="page" class="hfeed site">
            <header>
                <div class="site-header inside-page">

    <?php endif; ?>

                <div class="site-branding">
                    <h1 class="logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                            <?php if(is_front_page()) : ?>
                                <img src="<?php echo get_bloginfo('template_url') ?>/assets/img/mean-mule-distilling-co.svg" class="header-logo" title="<?php bloginfo( 'name' ); ?>">
                            <?php else: ?>
                                <img src="<?php echo get_bloginfo('template_url') ?>/assets/img/logo-no-text.svg" class="header-logo" title="<?php bloginfo( 'name' ); ?>">
                            <?php endif; ?>
                        </a>
                    </h1>
                </div>

                <nav id="site-navigation" class="site-navigation">
                  <div id="responsive-menu"><?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'menu-header', 'menu_class' => 'header-navigation' ) ); ?></div>
                </nav>

                <?php if(is_front_page()) : ?>
                    <div class="homepage-title">
                        <h1>American Agave Spirits</h1>
                        <h2>Distilled from 100% Blue Agave</h2>
                        <h3>Product of Kansas City, MO</h3>
                    </div>
                <?php endif; ?>
            </div>

        <?php if(is_front_page()) : ?>
            <div class="default-header-image" style="background-image: url(<?php echo get_bloginfo('template_url') ?>/assets/img/mean_mule_gold_aged_agave_spirit.jpg)"></div>
        <?php endif; ?>

    </header>
    <div id="wrap-main" class="wrap-main">
