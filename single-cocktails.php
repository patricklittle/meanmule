<?php get_header(); ?>
  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area">
        <main id="main" class="site-main">
        <?php if ( have_posts() ) {
          while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article" itemscope itemtype="http://schema.org/Recipe">
              <header class="page-header">
                <h1 itemprop="name"><?php the_title(); ?></h1>
                <meta itemprop="url" content="<?php the_permalink(); ?> " />
              </header>

              <div class="page-content">

                  <?php the_post_thumbnail(large, array('class' => 'single-cocktail-image', 'itemprop' => 'image')); ?>

                  <div class="single-cocktail-row">

                    <?php if( have_rows('ingredients') ): ?>
                        <div class="single-cocktail-ingredients">
                            <div class="wrap">
                                <h5>Ingredients</h5>
                                <ul class="ingredients">

                                <?php while( have_rows('ingredients') ): the_row(); ?>
                                  <li itemprop="ingredients"><?php the_sub_field('ingredient'); ?></li>
                                <?php endwhile; ?>

                                </ul>
                            </div>
                        </div>

                    <?php endif; ?>

                    <div class="single-cocktail-content">
                        <div class="wrap" itemprop="recipeInstructions">
                            <?php the_content(); ?>
                        </div>
                    </div>

                </div>

                <?php wp_link_pages(); ?>
              </div>
            </article>
          <?php endwhile;
        } else { ?>
          <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
              <h1><?php _e( 'Not found', 'meanmule' ); ?></h1>
            </header>
            <div class="page-content">
              <p><?php _e( 'Sorry, but your request could not be completed.', 'meanmule' ); ?></p>
              <?php get_search_form(); ?>
            </div>
          </article>
        <?php } ?>
        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
