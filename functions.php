<?php // ==== FUNCTIONS ==== //

// Load configuration defaults for this theme; anything not set in the custom configuration (above) will be set here
defined( 'meanmule_SCRIPTS_PAGELOAD' )       || define( 'meanmule_SCRIPTS_PAGELOAD', true );

// An example of how to manage loading front-end assets (scripts, styles, and fonts)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/assets.php' );

// Required to demonstrate WP AJAX Page Loader (as WordPress doesn't ship with even simple post navigation functions)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/navigation.php' );

// Only the bare minimum to get the theme up and running
function meanmule_setup() {

  // HTML5 support; mainly here to get rid of some nasty default styling that WordPress used to inject
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  add_theme_support( 'title-tag' );

  // $content_width limits the size of the largest image size available via the media uploader
  // It should be set once and left alone apart from that; don't do anything fancy with it; it is part of WordPress core
  global $content_width;
  $content_width = 960;

  // Register header and footer menus
  register_nav_menu( 'header', __( 'Header menu', 'meanmule' ) );
  register_nav_menu( 'footer', __( 'Footer menu', 'meanmule' ) );

}
add_action( 'after_setup_theme', 'meanmule_setup', 11 );

// Sidebar declaration
function meanmule_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main sidebar', 'meanmule' ),
    'id'            => 'sidebar-main',
    'description'   => __( 'Appears to the right side of most posts and pages.', 'meanmule' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ) );
}
add_action( 'widgets_init', 'meanmule_widgets_init' );

add_theme_support( 'post-thumbnails' );
add_image_size( 'homepagefeatures', 1200, 800, array( 'center', 'center'));

// Remove Protected Post Text
add_filter( 'protected_title_format', 'remove_protected_text' );
    function remove_protected_text() {
    return __('%s');
}

// Custom Post Types
include_once('inc/custom-post-types.php');
