
var gulp         = require('gulp');
var watch        = require('gulp-watch');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync');
var prefix       = require('gulp-autoprefixer');
var plumber      = require('gulp-plumber');
var uglify       = require('gulp-uglify');
var rename       = require("gulp-rename");
var imagemin     = require("gulp-imagemin");
var pngquant     = require('imagemin-pngquant');

/**
*
* Styles
* - Compile
* - Compress/Minify
* - Catch errors (gulp-plumber)
* - Autoprefixer
*
**/
gulp.task('sass', function() {
  gulp.src('src/scss/**/*.scss')
  // .pipe(watch('src/sass/*.scss'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(prefix('last 2 versions', '> 1%', 'ie 8', 'Android 2', 'Firefox ESR'))
  .pipe(plumber())
  .pipe(gulp.dest(''));
});

/**
*
* BrowserSync.io
* - Watch CSS, JS & HTML for changes
* - View project at: localhost:3000
*
**/
// gulp.task('browser-sync', function() {
//   browserSync.init(['public/css/*.css', 'public/js/**/*.js', 'index.html'], {
//     server: {
//       baseDir: './'
//     }
//   });
// });
//

/**
*
* Javascript
* - Uglify
*
**/
gulp.task('scripts', function() {
  gulp.src('src/js/*.js')
  // .pipe(watch('src/js/*.js'))
  .pipe(uglify())
  .pipe(rename({
    suffix: ".min",
  }))
  .pipe(gulp.dest('assets/js'))
});

/**
*
* Images
* - Compress them!
*
**/
gulp.task('images', function () {
  return gulp.src('src/img/*')
  // .pipe(watch('src/images/*'))
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    use: [pngquant()]
  }))
  .pipe(gulp.dest('assets/img'));
});

/**
*
* Icons
* - Creates Icon Font from SVG
* - https://www.npmjs.com/package/gulp-iconfont
*
**/
gulp.task('icons', function(){
  return gulp.src(['src/icons/*.svg'], {base: 'src/'})
    .pipe(iconfontCss({
      fontName: 'iconfont',
      targetPath: '../../src/sass/base/_icons.scss',
      fontPath: '../fonts/'
    }))
    .pipe(iconfont({
      fontName: 'iconfont', // required
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
    }))
      .on('glyphs', function(glyphs, options) {
        // CSS templating, e.g.
        console.log(glyphs, options);
      })
    .pipe(gulp.dest('assets/fonts/'));
});

/**
*
* Default task
* - Runs sass, browser-sync, scripts and image tasks
* - Watchs for file changes for images, scripts and sass/css
*
**/
gulp.task('default', ['sass', 'scripts', 'images'], function () {
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('src/js/**/*.js', ['scripts']);
  gulp.watch('src/img/*', ['images']);
 // gulp.watch('src/icons/*', ['icons']);
});
