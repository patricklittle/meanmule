<?php // ==== ASSETS ==== //

// Now that you have efficiently generated scripts and stylesheets for your theme, how should they be integrated?
// This file walks you through an approach I use but you are free to do this any way you like

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery-core' );
    wp_register_script( 'jquery-core', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1' );
    wp_deregister_script( 'jquery-migrate' );
    wp_register_script( 'jquery-migrate', "https://code.jquery.com/jquery-migrate-3.0.0.min.js", array(), '3.0.0' );
}

// Load header assets; this should include the main stylesheet as well as any mission critical scripts
function meanmule_assets_header() {

    // Header script loading is simplistic in this starter kit but you may want to change what file is loaded based on various conditions; check out the footer asset loader for an example
    $file = 'header.min';
    wp_enqueue_script( 'meanmule-header', get_stylesheet_directory_uri() . '/assets/js/' . $file . '.js', $deps = array('jquery'), filemtime( get_template_directory() . '/assets/js/' . $file . '.js' ), false );

    // Register and enqueue our main stylesheet with versioning based on last modified time
    wp_register_style( 'meanmule-style', get_stylesheet_uri(), $dependencies = array(), filemtime( get_template_directory() . '/style.css' ) );
    wp_enqueue_style( 'meanmule-style' );
}
add_action( 'wp_enqueue_scripts', 'meanmule_assets_header' );

// Load footer assets
function meanmule_assets_footer() {

    $file = 'footer.min';
    // wp_enqueue_script( 'meanmule-footer', get_stylesheet_directory_uri() . '/assets/js/' . $file . '.js', $deps = array('jquery'), filemtime( get_template_directory() . '/assets/js/' . $file . '.js' ), false );
    wp_enqueue_script('meanmule-footer', get_template_directory_uri() .'/assets/js/footer.min.js', array('jquery'), null, true);
}

add_action( 'wp_footer', 'meanmule_assets_footer' );

// Load assets on single content; useful for conditional loading of the core comments script, for example
function meanmule_assets_singular() {
    if ( !is_singular() )
    return;

    // Load core WordPress script for handling threaded comments where appropriate
    // This isn't really useful since comments aren't a feature of this simple theme but you get the idea
    if ( comments_open() && get_option('thread_comments') )
    wp_enqueue_script( 'comment-reply' );
}

add_action( 'wp_enqueue_scripts', 'meanmule_assets_singular' );
