<?php
// http://codex.wordpress.org/Function_Reference/register_post_type
function register_custom_post_types() {
	/*
	 * Duplicate this for each CPT.
	// --------------------------------
	// CPT Name
	// --------------------------------
	$labels = array(
		'name' => _x('Movies', 'post type general name'),
		'singular_name' => _x('Movie', 'post type singular name'),
		'add_new' => _x('Add New', 'movie'),
		'add_new_item' => __('Add New Movie'),
		'edit_item' => __('Edit Movie'),
		'new_item' => __('New Movie'),
		'view_item' => __('View Movie'),
		'search_items' => __('Search Movies'),
		'not_found' =>  __('No Movies found'),
		'not_found_in_trash' => __('No Movies found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Movies'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => array('slug' => 'movies'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'page-attributes')
	);
	register_post_type('portfolio', $args);
	*/


    // --------------------------------
	// Spirits
	// --------------------------------
	$labels = array(
		'name' => _x('Spirits', 'post type general name'),
		'singular_name' => _x('Spirit', 'post type singular name'),
		'add_new' => _x('Add New', 'spirit'),
		'add_new_item' => __('Add New Spirit'),
		'edit_item' => __('Edit Spirit'),
		'new_item' => __('New Spirit'),
		'view_item' => __('View Spirit'),
		'search_items' => __('Search Spirits'),
		'not_found' =>  __('No Spirits found'),
		'not_found_in_trash' => __('No Spirits found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Spirits'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => array('slug' => 'spirits'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'menu_icon' => 'dashicons-filter',
		'supports' => array('title', 'page-attributes', 'thumbnail', 'editor')
	);
	register_post_type('spirit', $args);



	// --------------------------------
	// Cocktails
	// --------------------------------
	$labels = array(
		'name' => _x('Cocktails', 'post type general name'),
		'singular_name' => _x('Cocktail', 'post type singular name'),
		'add_new' => _x('Add New', 'cocktail'),
		'add_new_item' => __('Add New Cocktail'),
		'edit_item' => __('Edit Cocktail'),
		'new_item' => __('New Cocktail'),
		'view_item' => __('View Cocktail'),
		'search_items' => __('Search Cocktails'),
		'not_found' =>  __('No Cocktails found'),
		'not_found_in_trash' => __('No Cocktails found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Cocktails'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => array('slug' => 'cocktails'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'menu_icon' => 'dashicons-filter',
		'supports' => array('title', 'page-attributes', 'thumbnail', 'editor')
	);
	register_post_type('cocktails', $args);


}
add_action( 'init', 'register_custom_post_types' );
