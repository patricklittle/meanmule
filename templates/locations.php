<?php /* Template Name: Locations */ ?>

<?php get_header(); ?>
<div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
        <section id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="page-content">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class('section-article our-story'); ?> role="article">
                            <header class="page-header">
                                <h1><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                            </header>
                            <?php the_content(); ?>
                        </article>

                        <form action="#" method="post" class="location-form">
                            <label for="userAddress" class="address">
                                <input name="userAddress" id="userAddress" type="text" placeholder="Your Zip Code" value=""/>
                            </label>
                            <input type="button" class="button" id="submitLocationSearch" value="Search">
                        </form>

                        <p id="location-search-alert"></p>

                        <div id="locations-near-you-map"></div>

                        <div id="locations-near-you"></div>

                    <?php endwhile; ?>
                </div>
            </main>
        </section>
    </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/tabletop.js/1.5.1/tabletop.min.js'></script>
<script defer src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyBHSu_MagVsTZpd0j6ZHQBkum1A1YHSMP8&callback=createSearchableMap"></script>

<?php get_footer(); ?>
