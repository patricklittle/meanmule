<?php /* Template Name: Our Story */ ?>

<?php get_header(); ?>
  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area">
        <main id="main" class="site-main">
        <?php while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article our-story'); ?> role="article">
              <header class="page-header">
                <h1><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
              </header>
              <?php the_content(); ?>
            </article>
          <?php endwhile; ?>
        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
