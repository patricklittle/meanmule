<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>

<div class="homepage">

    <?php
    $args = array( 'post_type' => 'post', 'posts_per_page' => 1 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <section class="section-row section-img-right">
        <div class="section-image">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_post_thumbnail('large');?>
            </a>
        </div>
        <div class="section-content">
            <div class="section-title">Latest News</div>
            <div class="section-article">
                <div class="article-wrap">
                    <h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>

                    <span class="entry-meta"><?php the_date('F j, Y')?></span>
                    <?php the_content();?>
                </div>
            </div>
        </div>
    </section>
    <?php endwhile; wp_reset_query(); ?>


    <?php
    $args = array( 'post_type' => 'cocktails', 'posts_per_page' => 1 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <section class="section-row section-img-left">
        <div class="section-image">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_post_thumbnail('homepagefeatures');?>
            </a>
        </div>
        <div class="section-content">
            <div class="section-title">Cocktail of the Month</div>
            <div class="section-article">
                <h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>

                <?php if( have_rows('ingredients') ): ?>

                    <h5>Ingredients</h5>
                    <ul class="ingredients">

                	<?php while( have_rows('ingredients') ): the_row(); ?>
                        <li><?php the_sub_field('ingredient'); ?></li>
                    <?php endwhile; ?>

                    </ul>

                <?php endif; ?>
            </div>
        </div>
    </section>
    <?php endwhile; wp_reset_query(); ?>

</div>

<?php get_footer(); ?>
